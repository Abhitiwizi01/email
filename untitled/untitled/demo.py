


import datetime
import pandas as pd
import sys
from os import listdir
from os.path import isfile, join
import xlwt
import xlrd

from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
from pandas.core.dtypes.inference import is_number
from pymongo import MongoClient
mc = MongoClient('localhost:27017')
db = mc.datadb


style = xlwt.XFStyle()
style.num_format_str = '#,###0.00'


mypath = '/home/mudit/PycharmProjects/untitled'
textfiles = [join(mypath, f) for f in listdir(mypath) if isfile(join(mypath, f)) and '.txt' in f]

for textfile in textfiles:
    f = open(textfile, 'r+')
    h=f.readlines()
    # print (h)
    row_list = []
    for row in h:
        # print ([row+1])
        row_list.append(row.split(','))

    labels = row_list[0]

    for i in range(14):
        labels.append(i)

    print(labels)

    for i in range(1,len(row_list[:])):
        row_list[i] = tuple(row_list[i])

    del row_list[0]

    print(row_list)
    tables_t= pd.DataFrame(row_list,columns=labels)
    tables_t.to_csv("tables_t.csv", sep=',', encoding='utf-8')

    # pd.DataFrame(columns=row_list[0])

